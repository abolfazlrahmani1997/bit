<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $user_id
 * @property double $balance
 */
class Account extends Model
{
    use HasFactory, SoftDeletes ,HasUlids;

    protected $fillable = ['user_id', 'balance'];

    public function transactions():HasMany
    {
        return  $this->hasMany(Transaction::class,'account_id','id');
    }
}
