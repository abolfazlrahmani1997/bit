<?php

use App\Domains\Transaction\Http\Controller\TransactionController;
use Illuminate\Support\Facades\Route;

Route::name('transaction.')->prefix('transactions')->group(function () {
    Route::post('/debit', [TransactionController::class, 'debit']);
    Route::post('/credit', [TransactionController::class, 'credit']);
});
