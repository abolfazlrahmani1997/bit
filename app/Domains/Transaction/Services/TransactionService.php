<?php

namespace App\Domains\Transaction\Services;

use App\Domains\Account\Contracts\Repositories\AccountRepositoryInterface;
use App\Domains\Transaction\Contracts\Repositories\TransactionRepositoryInterface;
use App\Domains\Transaction\Contracts\Services\TransactionServiceInterface;
use App\Models\Account;
use App\Models\Transaction;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TransactionService implements TransactionServiceInterface
{

    public function __construct(
        private readonly AccountRepositoryInterface $accountRepository,
        private readonly TransactionRepositoryInterface $transactionRepository)
    {

    }


    public function debit(array $data): Transaction|bool
    {
        $account = $this->accountRepository->get(id: $data['account_id']);
//if handle by  strategy pattern in  scope similar to contex
        if ($this->debitable(account: $account, ammount: $data['amount'])) {
            try {

                return Cache::lock('transaction' . $account->id)->get(function () use ($account, $data) {
                    return DB::transaction(function () use ($account, $data) {
                        $transaction = $this->transactionRepository
                            ->create(['amount' => $data['amount']
                                , 'account_id' => $data['account_id']
                                , 'type' => 'debit']);
                        $this->accountRepository->update(account: $account, data: ['balance' => $account->balance - $data['amount']]);
                        return $transaction;
                    });
                });

            } catch (\Throwable $e) {

                return false;
            }

        }
        return false;

    }

    public function credit(array $data): Transaction|false
    {
        $account = $this->accountRepository->get(id: $data['account_id']);
        try {
            return Cache::lock('transaction' . $account->id)->get(function () use ($account, $data) {
                return DB::transaction(function () use ($account, $data) {
                    $transaction = $this->transactionRepository
                        ->create(['amount' => $data['amount']
                            , 'account_id' => $data['account_id']
                            , 'type' => 'credit']);
                    $this->accountRepository->update(account: $account, data: ['balance' => $account->balance + $data['amount']]);
                    return $transaction;
                });
            });

        } catch (\Throwable $e) {

            return false;
        }



    }
    private function debitable(Account $account, int $ammount): bool
    {
        return ($account->balance - $ammount) > 0;
    }
}
