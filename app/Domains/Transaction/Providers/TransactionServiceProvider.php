<?php

namespace App\Domains\Transaction\Providers;

use App\Domains\Account\Contracts\Repositories\AccountRepositoryInterface;
use App\Domains\Account\Contracts\Services\AccountServiceInterface;
use App\Domains\Account\Repositories\AccountRepository;
use App\Domains\Account\Services\AccountService;
use App\Domains\Transaction\Contracts\Repositories\TransactionRepositoryInterface;
use App\Domains\Transaction\Contracts\Services\TransactionServiceInterface;
use App\Domains\Transaction\Repositories\TransactionRepository;
use App\Domains\Transaction\Services\TransactionService;

class TransactionServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function register()
    {
        $this->app->bind(TransactionServiceInterface::class, TransactionService::class);
        $this->app->bind(TransactionRepositoryInterface::class, TransactionRepository::class);
    }
}
