<?php

namespace App\Domains\Transaction\Http\Controller;

use App\Domains\Account\Contracts\Services\AccountServiceInterface;
use App\Domains\Transaction\Contracts\Services\TransactionServiceInterface;
use App\Domains\Transaction\Http\Requests\CreateTransactionRequest;
use App\Domains\Transaction\Http\Requests\CreditTransactionRequest;
use App\Domains\Transaction\Http\Requests\DebitTransactionRequest;
use App\Domains\User\Contracts\Services\UserServiceInterface;
use App\Helper\ResponseWrapper;
use App\Http\Resources\TransactionResource;
use Illuminate\Routing\Controller;

class TransactionController extends Controller
{
    public function __construct(
        private readonly TransactionServiceInterface $transactionService,
        private readonly ResponseWrapper $responseWrapper
    )
    {
    }

    public function debit(DebitTransactionRequest $request): ResponseWrapper
    {

        return $this->responseWrapper
            ->setResource(TransactionResource::class)
            ->setData($this->transactionService
                ->debit($request->validated()));
    }

    public function credit(CreditTransactionRequest $request): ResponseWrapper
    {

        return $this->responseWrapper
            ->setResource(TransactionResource::class)
            ->setData($this->transactionService
                ->debit($request->validated()));

    }

    public function update()
    {

    }
}
