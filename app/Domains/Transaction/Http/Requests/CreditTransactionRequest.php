<?php

namespace App\Domains\Transaction\Http\Requests;
class CreditTransactionRequest extends \App\Http\Requests\Factory\ApiFactoryRequest
{

    public function rules(): array
    {
        return [
            'amount' => ['required', 'numeric'],
            'account_id' => ['required']
        ];
    }

    public function account_id(): string
    {
        return $this->validated('account_id');
    }

    public function user_id(): string
    {
        return $this->header('x-user-id');
    }

}
