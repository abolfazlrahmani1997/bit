<?php

namespace App\Domains\Transaction\Repositories;




use App\Domains\Transaction\Contracts\Repositories\TransactionRepositoryInterface;
use App\Models\Account;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class TransactionRepository implements TransactionRepositoryInterface
{

    public function get(string $id, bool $with_relation = false): Transaction
    {
        return $this->getQueryBuilder(with_relation: $with_relation)
            ->where('id', '=', $id)
            ->firstOrFail();
    }

    public function getAll(string $id, bool $with_relation = false): Collection
    {
        return $this->getQueryBuilder(with_relation: $with_relation)
            ->get();
    }

    public function create(array $data): Transaction
    {
        return Transaction::query()->create($data);
    }

    public function update(Transaction $account, array $data): bool
    {

        try {
            $account = DB::transaction(fn() => $account->update($data));

        } catch (QueryException $e) {

        }
        return $account;
    }

    public function delete(Transaction $account): bool
    {
        return $account->delete();
    }


    private function getQueryBuilder(bool $with_relation = false): \Illuminate\Database\Eloquent\Builder
    {
        return Transaction::query()->when($with_relation, function (\Illuminate\Database\Eloquent\Builder $query) {
            return $query->with([]);
        });

    }
}
