<?php
namespace App\Domains\Transaction\Contracts\Services;

use App\Models\Account;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;

interface TransactionServiceInterface
{

    public function debit(array $data):  Transaction|bool;

    public function credit(array $data):Transaction|false;

}
