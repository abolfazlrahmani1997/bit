<?php

namespace App\Domains\Transaction\Contracts\Repositories;

use App\Models\Account;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;


interface TransactionRepositoryInterface
{
    public function get(string $id): Transaction;

    public function getAll(string $id): Collection;

    public function create(array $data): Transaction;

    public function update(Transaction $user, array $data): bool;

    public function delete(Transaction $user): bool;

}
