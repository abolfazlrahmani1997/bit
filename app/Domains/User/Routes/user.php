<?php

use App\Domains\User\Http\Controller\UserController;
use Illuminate\Support\Facades\Route;

Route::name('users')->prefix('users')->group(function () {
    Route::post('/', [UserController::class, 'create']);
});
