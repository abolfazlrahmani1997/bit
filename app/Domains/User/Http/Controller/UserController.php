<?php

namespace App\Domains\User\Http\Controller;

use App\Domains\User\Contracts\Services\UserServiceInterface;
use App\Domains\User\Http\Requests\CreateUserRequest;
use App\Helper\ResponseWrapper;
use App\Http\Resources\UserResource;
use Illuminate\Routing\Controller;

class UserController extends Controller
{
    public function __construct(
        private readonly ResponseWrapper $responseWrapper, private readonly UserServiceInterface $userService)
    {
    }

    public function create(CreateUserRequest $createUserRequest)
    {
        $result = $this->userService->store(data: $createUserRequest->validated());
        return $this->responseWrapper->setResource(UserResource::class)
            ->setData($result)
            ->generateSingleResponse();

    }
}
