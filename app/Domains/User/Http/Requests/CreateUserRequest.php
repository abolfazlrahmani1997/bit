<?php

namespace App\Domains\User\Http\Requests;

use App\Domains\User\Http\Requests\Factory\ApiFactoryRequest;

class CreateUserRequest extends ApiFactoryRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required'],
            'email' => ['required'],
        ];

    }


    public function getUserId(): string
    {
        return $this->header('x-user-id');
    }
}
