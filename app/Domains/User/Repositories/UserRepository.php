<?php

namespace App\Domains\User\Repositories;


use App\Domains\User\Contracts\Repositories\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{

    public function get(string $id, bool $with_relation = false): User
    {
        return $this->getQueryBuilder(with_relation: $with_relation)
            ->where('id', '=', $id)
            ->firstOrFail();
    }

    public function getAll(string $id, bool $with_relation = false): Collection
    {
        return $this->getQueryBuilder(with_relation: $with_relation)
            ->get();
    }

    public function create(array $data): User
    {
        return User::query()->create($data);
    }

    public function update(User $user, array $data): bool
    {

        try {
            $user = DB::transaction(fn() => $user->update($data));

        } catch (QueryException $e) {

        }
        return $user;
    }

    public function delete(User $user): bool
    {
        return $user->delete();
    }


    private function getQueryBuilder(bool $with_relation = false): \Illuminate\Database\Eloquent\Builder
    {
        return User::query()->when($with_relation, function (\Illuminate\Database\Eloquent\Builder $query) {
            return $query->with('');
        });

    }
}
