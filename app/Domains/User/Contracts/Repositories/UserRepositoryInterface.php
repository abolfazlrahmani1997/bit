<?php

namespace App\Domains\User\Contracts\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;


interface UserRepositoryInterface
{
    public function get(string $id): User;

    public function getAll(string $id): Collection;

    public function create(array $data): User;

    public function update(User $user,array $data): bool;

    public function delete(User $user): bool;

}
