<?php
namespace App\Domains\User\Contracts\Services;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface UserServiceInterface
{
    public function retrieve(string $id, bool $with_relation = false): User;

    public function getAll(bool $withRelation = false): Collection;

    public function store(array $data): User|false;
}
