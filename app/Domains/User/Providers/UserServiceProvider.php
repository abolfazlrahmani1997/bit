<?php

namespace App\Domains\User\Providers;

use App\Domains\Account\Contracts\Repositories\AccountRepositoryInterface;
use App\Domains\Account\Contracts\Services\AccountServiceInterface;
use App\Domains\Account\Repositories\AccountRepository;
use App\Domains\Account\Services\AccountService;
use App\Domains\Transaction\Contracts\Repositories\TransactionRepositoryInterface;
use App\Domains\Transaction\Contracts\Services\TransactionServiceInterface;
use App\Domains\Transaction\Repositories\TransactionRepository;
use App\Domains\Transaction\Services\TransactionService;
use App\Domains\User\Contracts\Repositories\UserRepositoryInterface;
use App\Domains\User\Contracts\Services\UserServiceInterface;
use App\Domains\User\Repositories\UserRepository;
use App\Domains\User\Services\UserService;

class UserServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function register()
    {
        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }
}
