<?php

namespace App\Domains\User\Services;

use App\Domains\User\Contracts\Repositories\UserRepositoryInterface;
use App\Domains\User\Contracts\Services\UserServiceInterface;

class UserService implements UserServiceInterface
{

    public function __construct(private readonly UserRepositoryInterface $userRepository)
    {

    }

    public function retrieve(string $id, bool $with_relation = false): \App\Models\User
    {
        return $this->userRepository->get(id: $id);
    }

    public function getAll(bool $withRelation = false): \Illuminate\Database\Eloquent\Collection
    {
        return $this->userRepository->getAll();
    }

    public function store(array $data): \App\Models\User|false
    {
        $user = $this->userRepository->create(data: $data);
        return $user;
    }
}
