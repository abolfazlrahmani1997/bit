<?php

namespace App\Domains\Account\Providers;

use App\Domains\Account\Contracts\Repositories\AccountRepositoryInterface;
use App\Domains\Account\Contracts\Services\AccountServiceInterface;
use App\Domains\Account\Repositories\AccountRepository;
use App\Domains\Account\Services\AccountService;

class AccountServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function register()
    {
        $this->app->bind(AccountRepositoryInterface::class, AccountRepository::class);
        $this->app->bind(AccountServiceInterface::class, AccountService::class);
    }
}
