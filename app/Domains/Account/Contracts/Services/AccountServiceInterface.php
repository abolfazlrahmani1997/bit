<?php

namespace App\Domains\Account\Contracts\Services;

use App\Models\Account;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface AccountServiceInterface
{


    public function get(string $id, bool $withRelation = false): Account;

    public function store(array $data): Account|false;

}
