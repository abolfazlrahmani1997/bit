<?php

namespace App\Domains\Account\Contracts\Repositories;

use App\Models\Account;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;


interface AccountRepositoryInterface
{
    public function get(string $id, bool $withRelation = false): Account;

    public function create(array $data): Account;

    public function update(Account $account, array $data): bool;

}
