<?php

use App\Domains\Account\Http\Controller\AccountController;
use Illuminate\Support\Facades\Route;

Route::name('accounts')->prefix('accounts')->group(function () {
    Route::get('/{id}', [AccountController::class, 'retrieve']);
    Route::post('/', [AccountController::class, 'create']);
    Route::put('/{id}', [AccountController::class, 'update']);
});
