<?php

namespace App\Domains\Account\Http\Controller;

use App\Domains\Account\Contracts\Services\AccountServiceInterface;
use App\Domains\Account\Http\Requests\CreateAccountRequest;
use App\Helper\ResponseWrapper;
use App\Http\Resources\AccountResource;
use Illuminate\Routing\Controller;

class AccountController extends Controller
{
    public function __construct(
        private readonly AccountServiceInterface $accountService,
        private readonly ResponseWrapper $responseWrapper
    )
    {
    }


    public function retrieve(string $id): \App\Models\Account
    {
        return $this->accountService->get(id: $id, withRelation: true);

    }

    public function create(CreateAccountRequest $request)
    {
        $result = $this->accountService->store(data: ['user_id' => $request->getUserId()]);
        return $this->responseWrapper
            ->setData(data: $result)
            ->setResource(AccountResource::class)
            ->generateSingleResponse();

    }
}
