<?php

namespace App\Domains\Account\Http\Requests;

use App\Http\Requests\Factory\ApiFactoryRequest;

class CreateAccountRequest extends ApiFactoryRequest
{

    public function rules(): array
    {
        return [

        ];
    }

    public function getUserId(): string
    {
        return $this->header('x-user-id');
    }

}
