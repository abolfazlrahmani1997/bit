<?php

namespace App\Domains\Account\Repositories;


use App\Domains\Account\Contracts\Repositories\AccountRepositoryInterface;
use App\Domains\User\Contracts\Repositories\UserRepositoryInterface;
use App\Models\Account;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class AccountRepository implements AccountRepositoryInterface
{

    public function get(string $id, bool $withRelation = false): Account
    {
        return $this->getQueryBuilder(with_relation: $withRelation)
            ->where('id', '=', $id)
            ->firstOrFail();
    }


    public function create(array $data): Account
    {
        return Account::query()->create($data);
    }

    public function update(Account $account, array $data): bool
    {

        try {
            $account = DB::transaction(fn() => $account->update($data));

        } catch (QueryException $e) {

        }
        return $account;
    }

    public function delete(Account $account): bool
    {
        return $account->delete();
    }


    private function getQueryBuilder(bool $with_relation = false): \Illuminate\Database\Eloquent\Builder
    {
        return Account::query()->when($with_relation, function (\Illuminate\Database\Eloquent\Builder $query) {
            return $query->with('transactions');
        });

    }
}
