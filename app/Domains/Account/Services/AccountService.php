<?php

namespace App\Domains\Account\Services;

use App\Domains\Account\Contracts\Repositories\AccountRepositoryInterface;
use App\Domains\Account\Contracts\Services\AccountServiceInterface;
use App\Domains\User\Contracts\Repositories\UserRepositoryInterface;
use App\Domains\User\Contracts\Services\UserServiceInterface;
use App\Models\Account;
use Illuminate\Database\Eloquent\Collection;

class AccountService implements AccountServiceInterface
{

    public function __construct(private readonly AccountRepositoryInterface $accountRepository)
    {

    }

    public function store(array $data): Account|false
    {
        return $this->accountRepository->create(data: $data);
    }

    public function get(string $id, bool $withRelation = false): Account
    {
        return $this->accountRepository->get(id: $id, withRelation: $withRelation);
    }
}
